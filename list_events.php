<style>
.fields {
	padding: 3px 8px;
	font-size: 1.7em;
	line-height: 100%;
	width: 100%;
	outline: 0;
}
.success {
	border-left: 4px solid #7ad03a;
	padding: 12px 12px;
	background-color: #fff;
	-webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
	box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
}
.errors {
	border-left: 4px solid #FE0606;
	padding: 12px 12px;
	background-color: #fff;
	-webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
	box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);	
	margin: 9px 21px 0 0;
}

</style>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places&amp;dummy=.js"></script>
<?php
	require('event_function.php');

	global $wpdb;
	$action = $_GET['action'];
	$id = $_GET['id'];
	$where = array('id' => $id );
	$table_name 	= $wpdb->prefix . 'events';
	$category_table_name = $wpdb->prefix . 'categories';
	
	/**
	 * CHECK ACTION EQUALS EDIT 
	 * DISPLAY EDIT FORM 
	 */
	if( !empty($action) && $action == 'edit'   && !empty($id) ) { 
		/**
		 * IF USER SUBMIT FORM 
		 * WE WILL VALIDATE FIELDS 
		 * IF ERROR WE WILL NOT EXECUTE UPDATE QUERY 
		 */
		if( isset($_POST['submit'] ) ) {

			$category			= $_POST['category'];
			$title 				= $_POST['title'];
			$start_date 		= $_POST['start-date'];
			$end_date 			= $_POST['end-date'];
			$description 		= $_POST['event_description'];
			$address1			= $_POST['address1'];
			$address2			= $_POST['address2'];
	  		$city				= $_POST['city'];
	  		$state				= $_POST['state'];
	  		$zip				= $_POST['zip'];
	  		$fileToUpload  	= $_POST["thumbnail"];
			$reg_errors = validate_fields($category, $title, $start_date, $end_date, $description, $address1, $address2, $city, $state, $zip);

			if(isset( $_FILES["fileToUpload"]["name"]) && !empty($_FILES["fileToUpload"]["name"]) ) {
				$target = wp_upload_dir();
				$target_dir = $target['path'];
				$fileToUpload = $target['url']. "/". $_FILES["fileToUpload"]["name"];
				$target_file = $target_dir . "/". basename($_FILES["fileToUpload"]["name"]);
				$uploadOk = 1;
				$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
			    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
			    // if($check !== false) {
			    //     // echo "File is an image - " . $check["mime"] . ".";
			    //     $uploadOk = 1;
			    // } else {
			    //     echo "File is not an image.";
			    //     $uploadOk = 0;
			    // }
			    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
		        // echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
			    } else {
			        $reg_errors->add('invalid_image', 'Invalid Image');
			    }
			}

			if ( count($reg_errors->get_error_messages()) ) {
			    foreach ( $reg_errors->get_error_messages() as $error ) {
			        echo '<div class="errors">';
			        echo '<strong>ERROR</strong>:';
			        echo $error . '<br/>';
			        echo '</div>';
			    }
			}
			else { 
				
				$data = array(
		        	'category_id'	=> $category,
		        	'title' 		=> $title,
		        	'description' 	=> $description,
		        	'start_date' 	=> $start_date,
		        	'end_date' 		=> $end_date,
		        	'address1' 		=> $address1,
		        	'address2' 		=> $address2,
		        	'city' 			=> $city,
		        	'state' 		=> $state,
		        	'zip' 			=> $zip,
		        	'thumbnail' 	=> $fileToUpload,
		        );
		        
		        
				$status = $wpdb->update( $table_name, $data, $where );
				if($status){
					echo '<div class="success">';
			        echo '<strong>Event updated successfully.</strong>:';
			        echo '</div>';
				} else {
					echo '<div class="errors">';
			        echo '<strong>Unexpected happens.</strong>:';
			        echo '</div>';
				}
			}

			
		}
		$res = $wpdb->get_row( 'SELECT * FROM '. $table_name. ' where id ='. $id);
		$query = 'SELECT id, title FROM '. $category_table_name;
		$categories = $wpdb->get_results( $query );
		?>
		<div class="wrap">
			<form action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="post" enctype="multipart/form-data">
			    <div id="titlewrap">
				    <h3>Select Category <strong>*</strong></h3>
					<select name="category">
						<?php 
							foreach ($categories as $row) {
								if( $res->category_id == $row->id ) {
									echo "<option value=". $row->id . " selected>". $row->title . "</option>";
								} else {
									echo "<option value=". $row->id . ">". $row->title . "</option>";
								}
							}
						?>
					</select>
			    </div>	
			    <div id="titlewrap">
				    <h3>Title <strong>*</strong></h3>
					<input type="text" class="fields" name="title" placeholder="Enter title here" value="<?php echo $res->title; ?>"  />
			    </div>
				<div>
					<h3>Description <strong>*</strong></h3>
			    	<?php
			    		// var_dump($res);
						$content = $res->description;
						// var_dump($content);
						$editor_id = 'event_description';
						wp_editor( $content, $editor_id );
					?>
			    </div>
			    <div>
				    <h3>Start Date <strong>*</strong></h3>
					<input type="date"  class="fields" name="start-date" value="<?php echo $res->start_date; ?>" />
			    </div>
			    <div>
				    <h3>End Date <strong>*</strong></h3>
					<input type="date"  class="fields" name="end-date" value="<?php echo $res->end_date; ?>" />
			    </div>
			    <div>
				    <h3>Address1 <strong>*</strong></h3>
					<input type="text"  class="fields" placeholder="Enter address1 here" name="address1" value="<?php echo $res->address1; ?>" />
			    </div>
			    <div>
				    <h3>Address2 <strong>*</strong></h3>
					<input type="text"  class="fields" placeholder="Enter address2 here" name="address2" value="<?php echo $res->address2; ?>" />
			    </div>
			    <div>
				    <h3>City <strong>*</strong></h3>
					<input type="text"  class="fields" onblur="getState(this.value)" id="city" name="city" placeholder="Enter city here" value="<?php echo $res->city; ?>" />
			    </div>
			    <div>
				    <h3>State <strong>*</strong></h3>
					<input type="text"  class="fields" id="state" name="state" placeholder="Enter state here" value="<?php echo $res->state; ?>"/>
			    </div>
			    <div>
				    <h3>Zip <strong>*</strong></h3>
					<input type="text" class="fields" id="zip" name="zip" placeholder="Enter zip here" value="<?php echo $res->zip; ?>"/>
			    </div>
			    <div>
			    	<h3>Thumbnail <strong>*</strong></h3>
			    	<img src="<?php echo $res->thumbnail; ?>" width="300" height="300" >
			    	<input type="hidden" name="thumbnail" value="<?php echo $res->thumbnail; ?>">
			    </div>
			    <div id="file-upload">
			    	<h3>Select image to Change <strong>*</strong></h3>
			    	<input type="file" class="fields" style="margin-bottom:20px"  name="fileToUpload" id="fileToUpload">
			    </div>
			    <input type="submit"  class="button button-primary button-large" name="submit" value="Update Event"/>
			</form>	
		</div>
		<?php
		
	}

	/**
	 * CHECK ACTION EQUALS DELETE
	 * DISPLAY EDIT FORM 
	 */
	if( $action == 'delete' && !empty($action) && !empty($id) ) { 
		$wpdb->delete( $table_name, $where);
	}
	// default 
	$sort = 'asc';
	$alice = 'e.';
	$query = 'select e.id, c.title as category_title, e.title, e.start_date, e.end_date, e.description FROM '. $table_name. ' as e, '. $category_table_name . ' as c where c.id = e.category_id';
	// var_dump($query);
	if ( isset($_GET['asc']) ) {
		if( $_GET['asc'] == 'category_title' ) $alice = 'c.';
		$query .= ' order by '. $alice . $_GET['asc'];
		$sort = 'desc';	
	} 
	else if ( isset($_GET['desc']) ) {
		if( $_GET['desc'] == 'category_title' ) $alice = 'c.';
		$query .= ' order by '. $alice . $_GET['desc']. ' desc';
		$sort = 'asc';
	}

	// date filtering
	if ( isset($_POST['filter']) ) {
		if ( isset($_POST['from-date']) ) {
			$query .= ' and  "'. $_POST['from-date']. '" >= e.start_date and ';
		}
		if ( isset($_POST['after-date']) ) {
			$query .= ' "'. $_POST['after-date']. '" <= e.end_date' ;
		}
	}
	var_dump($query);
	$results = $wpdb->get_results( $query );
	// var_dump($results);
	$url = menu_page_url( 'list_events', false);

?>

<?php
$action = @$_GET['action'];
if (!isset($action)){
?>
	<!-- Date filter -->
	<div class="wrap" style="margin-bottom:30px">
		<form action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="post" >
			<table class="widefat">
				<tbody>
					<tr>
						<td>
							<h3>From Date</h3>
						</td>
						<td>
							<input type="date"  style="width:70%" name="from-date" />
						</td>
					</tr>
					<tr>
						<td>
							<h3>After Date</h3>
						</td>
						<td>
							<input type="date"  style="width:70%" name="after-date" />
						</td>
					</tr>
					<tr>
						<td>
							<input type="submit" value="Filter" name="filter" class="button button-primary button-large">
						</td>
					</tr>
				</tbody>
			</table>
		</form>
	</div>
	<!-- Date filter -->

	<div class="wrap">
		<table class="widefat">
			<thead>
				<tr>
					<th><a href="<?php echo $url. "&&". $sort. "=category_id"  ?>">Category</a></th>
					<th><a href="<?php echo $url. "&&". $sort. "=title"  ?>">Title</a></th>
					<th><a href="<?php echo $url. "&&". $sort. "=start_date"  ?>">Start Date</a></th>
					<th><a href="<?php echo $url. "&&". $sort. "=end_date"  ?>">End Date</a></th>
					<th><a href="<?php echo $url. "&&". $sort. "=description"  ?>">Description</a></th>
					<th>Action</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th><a href="<?php echo $url. "&&". $sort. "=category_id"  ?>">Category</a></th>
					<th><a href="<?php echo $url. "&&". $sort. "=title"  ?>">Title</a></th>
					<th><a href="<?php echo $url. "&&". $sort. "=start_date"  ?>">Start Date</a></th>
					<th><a href="<?php echo $url. "&&". $sort. "=end_date"  ?>">End Date</a></th>
					<th><a href="<?php echo $url. "&&". $sort. "=description"  ?>">Description</a></th>
					<th>Action</th>
				</tr>
			</tfoot>
			<tbody>
						<?php
							foreach ($results as $row) {
								echo "<tr>";
									echo "<td>". $row->category_title. "</td>";
									echo "<td>". $row->title. "</td>";
									echo "<td>". $row->start_date. "</td>";
									echo "<td>". $row->end_date. "</td>";
									echo "<td>". $row->description. "</td>";
									echo "<td>". 
											"<a href=".
											$url.
											"&&action=edit".
											"&&id=". $row->id.
											">Edit</a> ||". 
											"<a  id='delete' href=".
											$url.
											"&&action=delete".
											"&&id=". $row->id.
											">Delete</a>";
									echo "</td>";
								echo "</tr>";
							}
						?>
			</tbody>
		</table>
	</div>
<?php
}
?>
<script>

	var input = document.getElementById('city');
	 var options = {
	  types: ['(cities)']
	 };
	new google.maps.places.Autocomplete(input, options);

	var input = document.getElementById('state');
	 var options = {
	  types: ['(states)']
	 };
	new google.maps.places.Autocomplete(input, options);

	var input = document.getElementById('zip');
	 var options = {
	  types: ['geocode']
	 };
	new google.maps.places.Autocomplete(input, options);

	function getState(citiesText) {
		var arr = citiesText.split(',');
		if(arr.length == 1 ) {
			document.getElementById('state').value = arr[0]
		} else if ( arr.length > 1 ) {
			document.getElementById('state').value = arr[1];		
		}
		
	}

</script>

