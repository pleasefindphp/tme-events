<style type="text/css">
.fields {
	padding: 3px 8px;
	font-size: 1.7em;
	line-height: 100%;
	width: 100%;
	outline: 0;
}
.success {
	border-left: 4px solid #7ad03a;
	padding: 12px 12px;
	background-color: #fff;
	-webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
	box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
}
.errors {
	border-left: 4px solid #FE0606;
	padding: 12px 12px;
	background-color: #fff;
	-webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
	box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);	
	margin: 9px 21px 0 0;
}
</style>
<?php
	global $wpdb;
	global $reg_errors;
	$reg_errors = new WP_Error;

	$id = $_GET['id'];
	$table_name 	= $wpdb->prefix . 'events';
	$attendance_table_name 	= $wpdb->prefix . 'attendance';
	$res = $wpdb->get_row( 'SELECT * FROM '. $table_name. ' where id ='. $id);
	$url = $_SERVER['REQUEST_URI'];
	// var_dump($_GET['action']);
	if ( isset($_POST['submit']) ) {

		$data = array(
			'name' 	=> $_POST['username'],
			'email' 	=> $_POST['email'],
			'contact' 	=> $_POST['contact'],
			'address' 	=> $_POST['address'],
			'message' 	=> $_POST['message'],
			'event_id' 	=> $_POST['event_id']
		);

		if( empty($data['name']) ) $reg_errors->add('username', 'Required form username is missing');
		if( empty($data['email']) ) $reg_errors->add('email', 'Required form email is missing');
		if( empty($data['contact']) ) $reg_errors->add('contact', 'Required form contact is missing');
		if( empty($data['address']) ) $reg_errors->add('address', 'Required form address is missing');
		if( empty($data['message']) ) $reg_errors->add('message', 'Required form message is missing');
		if( empty($data['event_id']) ) $reg_errors->add('event_id', 'Required form event_id is missing');
		if ( !is_email( $data['email'] ) ) $reg_errors->add( 'email_invalid', 'Email is not valid' );

		if ( count($reg_errors->get_error_messages()) ) {
		    foreach ( $reg_errors->get_error_messages() as $error ) {
		        echo '<div class="errors" >';
		        echo '<strong>ERROR</strong>:';
		        echo $error . '<br/>';
		        echo '</div>';
		    }
		}
		else { 
	        $status = $wpdb->insert( $attendance_table_name, $data );
			// var_dump($status);
			if ( $status ) {
				echo '<div class="success">';
		        echo '<strong>Event created successfully.</strong>:';
		        echo '</div>';
			} else {
				echo '<div class="errors">';
		        echo '<strong>Unexpected happens.</strong>:';
		        echo '</div>';
			}

		}	

	} else if( $_GET['action'] == 'apply_form' ) {
?>
	<div >
	<form action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="post" enctype="multipart/form-data">
	    <div>
		    <h3>Name <strong>*</strong></h3>
			<input type="text" class="fields" name="username" placeholder="Enter name"  />
	    </div>
	    <div>
		    <h3>Email <strong>*</strong></h3>
			<input type="text" class="fields" name="email" placeholder="Enter email"  />
	    </div>
	    <div>
		    <h3>Contact <strong>*</strong></h3>
			<input type="text" class="fields" name="contact" placeholder="Enter Contact"  />
	    </div>
	    <div>
		    <h3>Address <strong>*</strong></h3>
			<input type="text" class="fields" name="address" placeholder="Enter address"  />
	    </div>
		<div>
			<h3>Message <strong>*</strong></h3>
	    	<textarea name="message"></textarea>
			<input type="hidden" name="event_id" value="<?php echo $id; ?>">
	    </div>
	    
	    <input type="submit"  class="button button-primary button-large" name="submit" value="Apply"/>
	</form>
<?php
	} 
	else {
?>
	<div class="wrap">
		<table class="widefat">
			<tbody>
				<tr>
					<td>Title:</td>
					<td><?php echo $res->title ?></td>
				</tr>
				<tr>
					<td>Description:</td>
					<td><?php echo $res->description ?></td>
				</tr>
				<tr>
					<td>Start Date:</td>
					<td><?php echo $res->start_date ?></td>
				</tr>
				<tr>
					<td>End Date:</td>
					<td><?php echo $res->end_date ?></td>
				</tr>
				<tr>
					<td>Address1:</td>
					<td><?php echo $res->address1 ?></td>
				</tr>
				<tr>
					<td>Address2:</td>
					<td><?php echo $res->address2 ?></td>
				</tr>
				<tr>
					<td>City:</td>
					<td><?php echo $res->city ?></td>
				</tr>
				<tr>
					<td>State:</td>
					<td><?php echo $res->state ?></td>
				</tr>
				<tr>
					<td>Zip:</td>
					<td><?php echo $res->zip ?></td>
				</tr>
				<tr>
					<td>Image:</td>
					<td><img src="<?php echo $res->thumbnail ?>"></td>
				</tr>
				<tr>
					<td><a href="<?php echo $url. '&&action=apply_form' ?>">Apply</a></td>
				</tr>
			</tbody>
		</table>
	</div>
<?php 
	}