<?php
/**
 * @package tme-events
 * @version 1
 */
/*
Plugin Name: Tme-Events
Plugin URI: http://way2onlinestuff.com
Description: This is event plugin 
Author: Ady Creaters
Version: 1
Author URI: http://way2onlinestuff.com
*/

// require_once('event_function.php');

add_action( 'admin_menu', 'register_my_custom_menu_page' );
//add_shortcode( 'tme_events', 'attendance_function' );

add_shortcode( 'tme_events', 'all_events' );



function all_events(){

	if(isset($_GET['id'])){
		require('single_event.php');
		return;
	} 
	
	$content = '';
	$content .= "<link rel='stylesheet' href='" . get_template_directory_uri() . "/css/bootstrap.css' type='text/css' media='all' />";
	$content .= "<link rel='stylesheet' href='" . get_template_directory_uri() . "/css/portfolio.css' type='text/css' media='all' />";
	global $wpdb;
	$url = $_SERVER['REQUEST_URI'];
	$table_name = $wpdb->prefix . 'events';
	$results = $wpdb->get_results( 'SELECT * FROM '. $table_name );

	$content .= '<section id="main" class="row portfolio-grid">';
	$content .= '<div class="col-md-12">';
		$content .= '<div class="row">';
			$content .= '<div class="col-md-6 col-sm-6 col-xs-6">';
				$content .= '<h2 class="content-title">Events</h2>';
			$content .= '</div>';
			$content .= '<div class="col-md-6 col-sm-6 col-xs-6">';
				$content .= '<div class="btn-group pull-right">';
					$content .= '<button type="button" class="btn btn-default" title="Grid"><i class="fa fa-th"></i></button>';
		      $content .= '<button type="button" class="btn btn-default" title="List"><i class="fa fa-th-list"></i></button>';
		    $content .= '</div>';
			$content .= '</div>';
		$content .= '</div>';
		if ( $results ) :
			$content .= '<div class="row">';
			$i == 0; foreach ($results as $row) {

				//var_dump($row);
					if (($i > 0) && ($i/3== 0)) {
						echo '</div><div class="row">';
					}
					$content .= '<div class="col-md-4">';
						$content .= '<div class="prt-details">';
							if ( $row->thumbnail ) {
								$content .= '<img src="'.$row->thumbnail.'" alt="" >';
							}
							$content .= '<div class="portfolio-details">';
								$content .= '<h2 class="content-title">'.$row->title.'</h2>';
								$content .= '<p>'.substr($row->description, 0, 100).'...</p>';
								$content .= '<a href="'. $_SERVER['REQUEST_URI']. '&&id='. $row->id. '" class="btn btn-warning">Read More</a>';
							$content .= '</div>';
						$content .= '</div>';
					$content .= '</div>';
			$i++; }
			wp_reset_postdata();
			$content .= '</div>';
		else:
			$content .= '<p>'. _e( "Sorry, no events matched your criteria." ) .'</p>';
		endif;
	$content .= '</div>';
	$content .= '</section>';
	$content .= '<script>';
		
		$content .= "$('.portfolio-grid .btn-group button').click(function(e){
			e.preventDefault();
			var m = $(this).attr('title');
			if (m == 'List') {
				$('.portfolio-grid').addClass('active');
			}else{
				$('.portfolio-grid').removeClass('active');
			}
		});
	</script>";

	return $content;
}



function attendance_function() {
	global $wpdb;
	$url = $_SERVER['REQUEST_URI'];
	$table_name = $wpdb->prefix . 'events';
	$results = $wpdb->get_results( 'SELECT * FROM '. $table_name );
	?>
	<div class="wrap">
		<table class="widefat">
			<thead>
				<tr>
					<th>Title</th>
					<th>Date</th>
					<th>Description</th>
					<th>Action</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th>Title</th>
					<th>Date</th>
					<th>Description</th>
					<th>Action</th>
				</tr>
			</tfoot>
			<tbody>
						<?php
							foreach ($results as $row) {
								echo "<tr>";
									echo "<td>". $row->title. "</td>";
									// echo "<td>". $row->date. "</td>";
									echo "<td>". $row->description. "</td>";
									echo "<td>";
									echo "<a href=".
											$url.
											"&&action=apply".
											"&&event_id=". $row->id.
											">Apply</a>";
									echo "</td>";
								echo "</tr>";
							}
						?>
			</tbody>
		</table>
	</div>
	<?php

	if( $_GET['action'] == 'apply' && isset($_GET['event_id']) ) {
	?>
		<form action="<?php echo $url ?>" method="post">
			<div>
				Name:<input type="text" name="username">
			</div>
			
			<div>
				Email:<input type="text" name="email" >
			</div>
			<div>
				Contact:<input type="text" name="contact" >
			</div>
			<div>
				Address:<input type="text" name="address" >
			</div>
			<div>
			    Messsage:<textarea name="message"></textarea>
			    <input type="hidden" name="event_id" value="<?php echo $_GET['event_id']; ?>">
		    </div>

			<input type="submit" name="submit" value="Apply"/>
		</form>		
	<?php
		if ( isset($_POST['submit']) ) { 
			$name = $_POST['username'];
			$email = $_POST['email'];
			$contact = $_POST['contact'];
			$address = $_POST['address'];
			$message = $_POST['message'];
			$event_id = $_POST['event_id'];
			// echo $event_id;

			global $reg_errors;
			$reg_errors = new WP_Error;
			if ( empty( $name ) || empty( $email ) || empty( $contact ) ||  empty( $address ) || empty( $message ) ) $reg_errors->add('field', 'Required form field is missing');
			if ( !is_email( $email ) ) $reg_errors->add( 'email_invalid', 'Email is not valid' );
			
			if ( count($reg_errors->get_error_messages()) ) {
			    foreach ( $reg_errors->get_error_messages() as $error ) {
			        echo '<div>';
			        echo '<strong>ERROR</strong>:';
			        echo $error . '<br/>';
			        echo '</div>';
			    }
			}
			else { 
				echo '<div>';
		        echo '<strong>Applyed successfully.</strong>:';
		        echo '</div>';
		        $table_name = $wpdb->prefix . 'attendance';
		        $data = array(
		        	'event_id' 		=> $event_id,
		        	'name' 			=> $name,
		        	'email' 		=> $email,
		        	'contact' 		=> $contact,
		        	'address'		=> $address,
		        	'message' 		=> $message
		        );
				$wpdb->insert( $table_name, $data );
				// return;
			}
		}
	}
}


function register_my_custom_menu_page()
{
  add_menu_page( 'All Events', 'Events', 'manage_options',  'list_events', 'list_events_function', '' );
  add_submenu_page( 'list_events', 'Add new category', 'Add new category', 'manage_options', 'add_new_category', 'new_category_function', '' );
  add_submenu_page( 'list_events', 'List category', 'List category', 'manage_options', 'list_category', 'list_category_function', '' );
  add_submenu_page( 'list_events', 'Add new event', 'Add new event', 'manage_options', 'add_new_event', 'new_event_function', '' );
  add_submenu_page( 'list_events', 'Event Attendance', 'Event Attendance', 'manage_options', 'event_attendance', 'event_attendance', '' );
}

/**
 * List Events 
 */
function list_events_function(){
  require('list_events.php');
}

/**
 * WILL CREATE NEW EVENT VIEW
 */
function new_event_function() {
	require('new_event.php');
}

function new_category_function() {
	require( 'new_category.php' );
}

function list_category_function() {
	require( 'list_category.php' );
}


function event_attendance() {
	require('event_attendance.php');
}

global $wpdb;
require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

$category_table_name = $wpdb->prefix . 'categories';
$sql = "CREATE TABLE $category_table_name ( 
			id bigint(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
			title text NOT NULL, 
			description longtext NOT NULL
		) $charset_collate";
$charset_collate = $wpdb->get_charset_collate(); 
dbDelta( $sql );

$event_table_name = $wpdb->prefix . 'events';
$sql = "CREATE TABLE $event_table_name ( 
			id bigint(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
			category_id bigint(20) UNSIGNED NOT NULL,
			title text NOT NULL, 
			description longtext NOT NULL, 
			start_date date NOT NULL DEFAULT '0000-00-00 00:00:00', 
			end_date date NOT NULL DEFAULT '0000-00-00 00:00:00', 
			address1 VARCHAR(255) NOT NULL,
			address2 VARCHAR(255) NOT NULL,  
			city VARCHAR(255) NOT NULL, 
			state VARCHAR(50) NOT NULL, 
			zip VARCHAR(50)  NOT NULL, 
			thumbnail VARCHAR(255) NOT NULL
		) $charset_collate";
$charset_collate = $wpdb->get_charset_collate(); 
dbDelta( $sql );

$attendance_table_name = $wpdb->prefix . 'attendance';
$sql = "CREATE TABLE $attendance_table_name ( 
		id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
		event_id int(6), 
		name VARCHAR(30) NOT NULL, 
		email VARCHAR(30) NOT NULL, 
		contact BIGINT(30) NOT NULL,
		address VARCHAR(255) NOT NULL,
		message longtext NOT NULL, 
		present_status BOOLEAN  NOT NULL DEFAULT false 
) $charset_collate";
dbDelta( $sql );

