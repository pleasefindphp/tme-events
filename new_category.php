<style>
.fields {
	padding: 3px 8px;
	font-size: 1.7em;
	line-height: 100%;
	width: 100%;
	outline: 0;
}
.success {
	border-left: 4px solid #7ad03a;
	padding: 12px 12px;
	background-color: #fff;
	-webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
	box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
}
.errors {
	border-left: 4px solid #FE0606;
	padding: 12px 12px;
	background-color: #fff;
	-webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
	box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);	
	margin: 9px 21px 0 0;
}
</style>

<h1>New Category</h1>
<?php 

	if( isset($_POST['submit'] ) ) {
		global $wpdb;
		global $reg_errors;
		$reg_errors = new WP_Error;

		$title 			= $_POST['title'];
		$description 	= $_POST['event_description'];

		$table_name 	= $wpdb->prefix . 'categories';

		if( empty($title) ) $reg_errors->add('title', 'Required form title is missing');
		if( empty($description) ) $reg_errors->add('description', 'Required form description is missing');

		if ( count($reg_errors->get_error_messages()) ) {
		    foreach ( $reg_errors->get_error_messages() as $error ) {
		        echo '<div class="errors">';
		        echo '<strong>ERROR</strong>:';
		        echo $error . '<br/>';
		        echo '</div>';
		    }
		}
		else { 
			$data = array( 'title' => $title, 'description' => $description );

			$check = $wpdb->insert( $table_name, $data );
			if ( $check ) {
				echo '<div class="success">';
		        echo '<strong>Event created successfully.</strong>:';
		        echo '</div>';	
			} else {
				echo '<div class="errors">';
		        echo '<strong>Unexpected happens.</strong>:';
		        echo '</div>';	
			}
		}
	} 
?>
<div class="wrap">
	<form action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="post" enctype="multipart/form-data">
    <div id="titlewrap">
	    <h3>Title <strong>*</strong></h3>
		<input type="text" class="fields" name="title" placeholder="Enter title"  />
    </div>
	<div>
		<h3>Description <strong>*</strong></h3>
    	<?php
			$content = '';
			$editor_id = 'event_description';
			wp_editor( $content, $editor_id );
		?>
    </div>
    <input type="submit"  class="button button-primary button-large" name="submit" value="Add Category"/>
</form>	
</div>
