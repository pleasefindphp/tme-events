<style>
.fields {
	padding: 3px 8px;
	font-size: 1.7em;
	line-height: 100%;
	width: 100%;
	outline: 0;
}
.success {
	border-left: 4px solid #7ad03a;
	padding: 12px 12px;
	background-color: #fff;
	-webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
	box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
}
.errors {
	border-left: 4px solid #FE0606;
	padding: 12px 12px;
	background-color: #fff;
	-webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
	box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);	
	margin: 9px 21px 0 0;
}

</style>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places&amp;dummy=.js"></script>

<h1>New Event</h1>
<?php 

	/**
	 * Get All Categories
	 */
	require('event_function.php');

	$category_table_name = $wpdb->prefix . 'categories';
	$query = 'SELECT id, title FROM '. $category_table_name;
	$results = $wpdb->get_results( $query );

	if( isset($_POST['submit'] ) ) {

		$category			= $_POST['category'];
		$title 				= $_POST['title'];
		$start_date 		= $_POST['start-date'];
		$end_date 			= $_POST['end-date'];
		$description 		= $_POST['event_description'];
		$address1			= $_POST['address1'];
		$address2			= $_POST['address2'];
  		$city				= $_POST['city'];
  		$state				= $_POST['state'];
  		$zip				= $_POST['zip'];
		// $fileToUpload  	= $_POST["fileToUpload"];
		$table_name 	= $wpdb->prefix . 'events';
		$reg_errors = validate_fields($category, $title, $start_date, $end_date, $description, $address1, $address2, $city, $state, $zip);

		// var_dump($table_name);
		// var_dump($reg_errors);
		// upload media 
		if(isset( $_FILES["fileToUpload"]["name"] ) && $_FILES["fileToUpload"]["name"] != '') {
			$upload = wp_upload_dir();
			$target_dir = $upload['path'];
			$fileToUpload = $upload['url']. "/". $_FILES["fileToUpload"]["name"];
			$target_file = $target_dir . "/". basename($_FILES["fileToUpload"]["name"]);
			$uploadOk = 1;
			$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
		    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
		    // if($check !== false) {
		    //     // echo "File is an image - " . $check["mime"] . ".";
		    //     $uploadOk = 1;
		    // } else {
		    //     echo "File is not an image.";
		    //     $uploadOk = 0;
		    // }
		}
	    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
	        // echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
	    } else {
	        $reg_errors->add('invalid_image', 'Invalid Image');
	    }
	    // var_dump($reg_errors);
		if ( count($reg_errors->get_error_messages()) ) {
		    foreach ( $reg_errors->get_error_messages() as $error ) {
		        echo '<div class="errors" >';
		        echo '<strong>ERROR</strong>:';
		        echo $error . '<br/>';
		        echo '</div>';
		    }
		}
		else { 
	        $data = array(
	        	'category_id'	=> $category,
	        	'title' 		=> $title,
	        	'description' 	=> $description,
	        	'start_date' 	=> $start_date,
	        	'end_date' 		=> $end_date,
	        	'address1' 		=> $address1,
	        	'address2' 		=> $address2,
	        	'city' 			=> $city,
	        	'state' 		=> $state,
	        	'zip' 			=> $zip,
	        	'thumbnail' 	=> $fileToUpload,
	        );
	        
	        // var_dump($data);
	        // var_dump($table_name);
			
			$status = $wpdb->insert( $table_name, $data );
			// var_dump($status);
			if ( $status ) {
				echo '<div class="success">';
		        echo '<strong>Event created successfully.</strong>:';
		        echo '</div>';
			} else {
				echo '<div class="errors">';
		        echo '<strong>Unexpected happens.</strong>:';
		        echo '</div>';
			}
		}
	} 
?>
<div class="wrap">
	<form action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="post" enctype="multipart/form-data">
    
	<div id="titlewrap">
	    <h3>Select Category <strong>*</strong></h3>
		<select name="category">
			<?php 
				foreach ($results as $row) {
					echo "<option value=". $row->id . ">". $row->title . "</option>";
				}
			?>
		</select>
    </div>	
    <div id="titlewrap">
	    <h3>Title <strong>*</strong></h3>
		<input type="text" class="fields" name="title" placeholder="Enter title"  />
    </div>
	<div>
		<h3>Description <strong>*</strong></h3>
    	<?php
			$content = '';
			$editor_id = 'event_description';
			wp_editor( $content, $editor_id );
		?>
    </div>
    <div>
	    <h3>Start Date <strong>*</strong></h3>
		<input type="date"  class="fields" name="start-date" />
    </div>
    <div>
	    <h3>End Date <strong>*</strong></h3>
		<input type="date"  class="fields" name="end-date" />
    </div>
    <div>
	    <h3>Address1 <strong>*</strong></h3>
		<input type="text"  class="fields" placeholder="Enter address1" name="address1" />
    </div>
    <div>
	    <h3>Address2 <strong>*</strong></h3>
		<input type="text"  class="fields" placeholder="Enter address2" name="address2" />
    </div>
    <div>
	    <h3>City <strong>*</strong></h3>
		<input type="text"  class="fields" onblur="getState(this.value)" id="city" name="city" placeholder="Enter city" />
    </div>
    <div>
	    <h3>state <strong>*</strong></h3>
		<input type="text"  class="fields" id="state" name="state" placeholder="Enter state" />
    </div>
    <div>
	    <h3>zip <strong>*</strong></h3>
		<input type="text" class="fields" id="zip" name="zip" placeholder="Enter zip" />
    </div>
    <div>
    	<h3>Select image to upload <strong>*</strong></h3>
    	<input type="file" class="fields" style="margin-bottom:20px"  name="fileToUpload" id="fileToUpload">
    </div>
    <input type="submit"  class="button button-primary button-large" name="submit" value="Add Event"/>
</form>	
</div>
<script>

	var input = document.getElementById('city');
	 var options = {
	  types: ['(cities)']
	 };
	new google.maps.places.Autocomplete(input, options);

	var input = document.getElementById('state');
	 var options = {
	  types: ['(states)']
	 };
	new google.maps.places.Autocomplete(input, options);

	var input = document.getElementById('zip');
	 var options = {
	  types: ['geocode']
	 };
	new google.maps.places.Autocomplete(input, options);

	function getState(citiesText) {
		var arr = citiesText.split(',');
		if(arr.length == 1 ) {
			document.getElementById('state').value = arr[0]
		} else if ( arr.length > 1 ) {
			document.getElementById('state').value = arr[1];		
		}
		
	}

</script>