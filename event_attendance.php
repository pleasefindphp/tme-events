<h1>list application</h1>
<?php 
	
	global $wpdb;
	$url = menu_page_url( 'event_attendance', false);
	$attendance = $wpdb->prefix . 'attendance';
	$events = $wpdb->prefix . 'events';
	$where = array( 'id' => $_GET['id'] );

	if( $_GET['action'] == 'accept' && isset($_GET['id']) ){
		$data = array( 'present_status' => true ); 
		$status = $wpdb->update( $attendance, $data, $where );
	}

	if( $_GET['action'] == 'reject' && isset($_GET['id']) ){
		$data = array( 'present_status' => false ); 
		$status = $wpdb->update( $attendance, $data, $where );
	}

	

	$query = ' select a.present_status, a.id, a.email, e.title, a.name from '. $attendance. ' as a, '. $events. ' as e  where a.event_id = e.id';
	// var_dump($query);
	// sorting
	$asc = $_GET['asc'];
	$desc = $_GET['desc'];
	$as = 'e.';
	$sort = 'asc';

	if ( isset($asc) ) {

		if ($asc == 'email' || $asc == 'name') {
			$as = 'a.';
		}

		$query .= ' order by '. $as. $asc;
		$sort = 'desc';	
	} 
	else if ( isset($desc) ) {

		if ($desc == 'email' || $desc == 'name') {
			$as = 'a.';
		}

		$query .= ' order by '. $as. $desc. ' desc';
		$sort = 'asc';
	}


	// $results = $wpdb->get_results( 'SELECT * FROM '. $attendance );
	var_dump($query);
	$results = $wpdb->get_results( $query );

?>

<!-- Date filter -->
<!-- <div class="wrap" style="margin-bottom:30px">
	<form action="<?php //echo $_SERVER['REQUEST_URI'] ?>" method="post" >
		<table class="widefat">
			<tbody>
				<tr>
					<td>
						<h3>From Date</h3>
					</td>
					<td>
						<input type="date"  style="width:70%" name="from-date" />
					</td>
				</tr>
				<tr>
					<td>
						<h3>After Date</h3>
					</td>
					<td>
						<input type="date"  style="width:70%" name="after-date" />
					</td>
				</tr>
				<tr>
					<td>
						<input type="submit" value="Filter" name="filter" class="button button-primary button-large">
					</td>
				</tr>
			</tbody>
		</table>
	</form>
</div>
	Date filter -->


<div class="wrap">
	<table class="widefat">
		<thead>
			<tr>
				<th><a href="<?php echo $url. "&&". $sort. "=title"  ?>">Event Name</a></th>
				<th><a href="<?php echo $url. "&&". $sort. "=name"  ?>">Name</a></th>
				<th><a href="<?php echo $url. "&&". $sort. "=email"  ?>">Email</a></th>
				<th>Action</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<th><a href="<?php echo $url. "&&". $sort. "=title"  ?>">Event Name</a></th>
				<th><a href="<?php echo $url. "&&". $sort. "=name"  ?>">Name</a></th>
				<th><a href="<?php echo $url. "&&". $sort. "=email"  ?>">Email</a></th>
				<th>Action</th>
			</tr>
		</tfoot>
		<tbody>
					<?php
						foreach ($results as $row) {
							// var_dump($row);
							if(!$row->present_status){
								$action = 'accept';
								$text = 'Accept';
							}
							else {
								$action = 'reject';
								$text = 'Reject';
							}
							echo "<tr>";
								echo "<td>". $row->title. "</td>";
								echo "<td>". $row->name. "</td>";
								echo "<td>". $row->email. "</td>";
								echo "<td>". 
										"<a href=".
										$url.
										"&&action=". $action. 
										"&&id=". $row->id.
										">". $text. "</a>";
								echo "</td>";
							echo "</tr>";
						}
					?>
		</tbody>
	</table>
</div>