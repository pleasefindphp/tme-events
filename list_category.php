<style>
.fields {
	padding: 3px 8px;
	font-size: 1.7em;
	line-height: 100%;
	width: 100%;
	outline: 0;
}
</style>
<?php

	global $wpdb;
	$action = $_GET['action'];
	$id = $_GET['id'];
	$where = array('id' => $id );
	$url = menu_page_url( 'list_category', false);
	$table_name 	= $wpdb->prefix . 'categories';
	
	/**
	 * CHECK ACTION EQUALS EDIT 
	 * DISPLAY EDIT FORM 
	 */
	if( !empty($action) && $action == 'edit'   && !empty($id) ) { 
		/**
		 * IF USER SUBMIT FORM 
		 * WE WILL VALIDATE FIELDS 
		 * IF ERROR WE WILL NOT EXECUTE UPDATE QUERY 
		 */
		if( isset($_POST['submit'] ) ) {

			$title 			= $_POST['title'];
			$description 	= $_POST['category_description'];

			global $reg_errors;
			$reg_errors = new WP_Error;
			if( empty($title) ) $reg_errors->add('title', 'Required form title is missing');
			if( empty($description) ) $reg_errors->add('description', 'Required form description is missing');
						
			if ( count($reg_errors->get_error_messages()) ) {
			    foreach ( $reg_errors->get_error_messages() as $error ) {
			        echo '<div>';
			        echo '<strong>ERROR</strong>:';
			        echo $error . '<br/>';
			        echo '</div>';
			    }
			}
			else { 
				
				$data = array( 'title' => $title, 'description' => $description );
		        
				$status = $wpdb->update( $table_name, $data, $where );
				if($status) echo "<h1>Update is successful</h1>";
				else echo "Unexpected Error Please Try Again.";
			}
		}

		$res = $wpdb->get_row( 'SELECT * FROM '. $table_name. ' where id ='. $id);
		// var_dump($res);
		?>

		<!-- edit Category -->
		<div class="wrap">
			<form action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="post" enctype="multipart/form-data">
			    <div id="titlewrap">
				    <h3>Title <strong>*</strong></h3>
					<input type="text" class="fields" name="title" placeholder="Enter title here" value="<?php echo $res->title; ?>"  />
			    </div>
				<div>
					<h3>Description <strong>*</strong></h3>
			    	<?php
						$content = $res->description;
						$editor_id = 'category_description';
						wp_editor( $content, $editor_id );
					?>
			    </div>
			    <div>
			   		<input type="submit"  class="button button-primary button-large" name="submit" value="Update category"/> 	
			    </div>
				    
			</form>	
		</div>
		<!-- edit Category -->
		<?php
		
	}

	// delete Category
	if( $action == 'delete' && !empty($action) && !empty($id) ) { 
		$delete_status = $wpdb->delete( $table_name, $where);
	}
	// delete Category

	
	// default 
	$sort = 'asc';
	$query = 'SELECT * FROM '. $table_name;

	if ( isset($_GET['asc']) ) {
		$query .= ' order by '. $_GET['asc'];
		$sort = 'desc';	
	} 
	else if ( isset($_GET['desc']) ) {
		$query .= ' order by '. $_GET['desc']. ' desc';
		$sort = 'asc';
	}

	$results = $wpdb->get_results( $query );
	
?>

<?php
$action = @$_GET['action'];
if (!isset($action)){
?>
	

	<div class="wrap">
		<table class="widefat">
			<thead>
				<tr>
					<th><a href="<?php echo $url. "&&". $sort. "=title"  ?>">Title</a></th>
					<th><a href="<?php echo $url. "&&". $sort. "=description"  ?>">Description</a></th>
					<th>Action</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th><a href="<?php echo $url. "&&". $sort. "=title"  ?>">Title</a></th>
					<th><a href="<?php echo $url. "&&". $sort. "=description"  ?>">Description</a></th>
					<th>Action</th>
				</tr>
			</tfoot>
			<tbody>
						<?php
							foreach ($results as $row) {
								echo "<tr>";
									echo "<td>". $row->title. "</td>";
									echo "<td>". $row->description. "</td>";
									echo "<td>". 
											"<a href=".
											$url.
											"&&action=edit".
											"&&id=". $row->id.
											">Edit</a> ||". 
											"<a  id='delete' href=".
											$url.
											"&&action=delete".
											"&&id=". $row->id.
											">Delete</a>";
									echo "</td>";
								echo "</tr>";
							}
						?>
			</tbody>
		</table>
	</div>

<?php
}
?>