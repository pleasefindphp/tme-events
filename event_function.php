<?php 
	
	global $wpdb;
	global $reg_errors;
	$table_name = $wpdb->prefix . 'events';
	// echo $table_name;
	// global $wpdb;
	
	function validateDate($date, $format = 'Y-m-d') {
	    // $d = DateTime::createFromFormat($format, $date);
	    // return $d && $d->format($format) == $date;
	 //    var_dump($date);
		// var_dump($timestamp);
	    return $timestamp = strtotime($date);
	}

	function validate_fields($category, $title, $start_date, $end_date,  $description, $address1, $address2, $city, $state, $zip) { 
		$reg_errors = new WP_Error;
		// if ( empty( $title ) || empty( $date ) || empty( $description ) ) $reg_errors->add('field', 'Required form field is missing');
		if( empty($category) ) $reg_errors->add('category', 'Required form category is missing');
		if( empty($title) ) $reg_errors->add('title', 'Required form title is missing');
		if( empty($start_date) ) $reg_errors->add('start_date', 'Required form start date is missing');
		if( empty($end_date) ) $reg_errors->add('end_date', 'Required form end date is missing');
		if( $start_date > $end_date ) $reg_errors->add('invalid_date_range', 'Required form incorrect date range is missing');
		if( empty($description) ) $reg_errors->add('description', 'Required form description is missing');
		if( empty($address1) ) $reg_errors->add('address1', 'Required form address1 is missing');
		if( empty($address2) ) $reg_errors->add('address2', 'Required form address2 is missing');
		if( empty($city) ) $reg_errors->add('city', 'Required form city is missing');
		if( empty($state) ) $reg_errors->add('state', 'Required form state is missing');
		if( empty($zip) ) $reg_errors->add('zip', 'Required form zip is missing');
		if ( !empty( $date ) && !validateDate($date) ) $reg_errors->add('invalid_date', 'Invalid Date Format');
		return $reg_errors;
	}

	function validate_attendance($name, $email, $description) { 
		$reg_errors = new WP_Error;
		if ( empty( $name ) || empty( $email ) || empty( $description ) ) $reg_errors->add('field', 'Required form field is missing');
		if ( !is_email( $email ) ) $reg_errors->add( 'email_invalid', 'Email is not valid' );
		
		return $reg_errors;
	}



?>